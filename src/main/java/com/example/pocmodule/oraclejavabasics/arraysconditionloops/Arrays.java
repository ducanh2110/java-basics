package com.example.pocmodule.oraclejavabasics.arraysconditionloops;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 03/08/2023 - 09:21
 */
public class Arrays {
    public static void main(String[] args) {
        int[] ages = new int[3];
        System.out.println(java.util.Arrays.toString(ages));
    }
}
