package com.example.pocmodule.oraclejavabasics.arraysconditionloops;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 03/08/2023 - 14:49
 */
public class SwitchCaseWithoutBrake {
    public static void main(String[] args) {
        int measure = 3;
        String size = "S";
        switch (measure) {
            case 3: size = "S";
            case 6: size = "M";
            case 9: size = "L"; break;
            default: size = "X";
        }

        System.out.println(size);
    }
}
