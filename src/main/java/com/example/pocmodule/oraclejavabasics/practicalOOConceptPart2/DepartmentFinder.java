package com.example.pocmodule.oraclejavabasics.practicalOOConceptPart2;

import java.util.List;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 02/08/2023 - 16:26
 */
public class DepartmentFinder {

    public Department find(List<Department> departmentList, String id) {
        for (Department department : departmentList) {
            if (department.getId().equals(id)) return department;
        }
        throw new RuntimeException("Ko tim thay department phu hop");
    }
}
