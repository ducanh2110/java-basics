package com.example.pocmodule.oraclejavabasics.practicalOOConceptPart2;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 02/08/2023 - 16:16
 */
@Getter @Setter @AllArgsConstructor
@ToString
public class Employee {
    String name;
}
