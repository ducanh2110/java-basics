package com.example.pocmodule.oraclejavabasics.practicalOOConceptPart2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 02/08/2023 - 16:23
 */
public class Application {
    public static void main(String[] args) {
        List<Department> allDepartments = new ArrayList<>();
        Department department = new Department();
        department.setId("dep1");
        department.getEmployees().add(new Employee("DUC ANH"));
        department.getEmployees().add(new Employee("DUC EM"));

//        Department department2 = new Department();
//        department2.setId("dep2");
//        department2.setEmployees(Arrays.asList(new Employee("Duc Anh-1"), new Employee("Duc Em-1")));
//        allDepartments.add(department2);

        allDepartments.add(department);
        department.assignEmployee(new Employee("Tung mo"));

        DepartmentFinder departmentFinder = new DepartmentFinder();
        Department foundDepartment = departmentFinder.find(allDepartments, "dep1");
        System.out.println(foundDepartment.getEmployees());
    }
}
