package com.example.pocmodule.oraclejavabasics.practicalOOConceptPart2;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 02/08/2023 - 16:16
 */
@Getter @Setter
@ToString
public class Department {

    String id;
    List<Employee> employees = new ArrayList<>();

    public void assignEmployee(Employee employee) {
        this.employees.add(employee);
    }
}
