package com.example.pocmodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocModuleApplication.class, args);
    }

}
