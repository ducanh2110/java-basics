package com.example.pocmodule.javanotes;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 08/08/2023 - 09:33
 */
public class StringIntern {
    public static void main(String[] args) {
        String strObj = new String("Hello!"); String str = "Hello!";
        // The two string references point two strings that are equal
        if (strObj.equals(str)) {
            System.out.println("The strings are equal");

        }
        // The two string references do not point to the same object
        if (strObj != str) {
            System.out.println("The strings are not the same object");
        }
        // If we intern a string that is equal to a given literal, the result is // a string that has the same reference as the literal.
        String internedStr = strObj.intern();
        if (internedStr == str) {
            System.out.println("The interned string and the literal are the same object");
        }
    }
}
