package com.example.pocmodule.api;

import lombok.Data;
import org.springframework.boot.Banner.Mode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: anh.phamduc
 * @mail: anh.phamduc@ivnd.com.vn
 * @created: 02/08/2023 - 13:47
 */
@RequestMapping("/demo")
@RestController
public class ModelAttributeController {

    @Data
    static class ModelAttributeModel {
        String field;
        String value;
    }
    @GetMapping
    public void test(@ModelAttribute("mod") ModelAttributeModel model) {
        //TODO alias of modelAttribute not change the request http
        System.out.println(model.getField());
        System.out.println(model.getValue());
    }

}
